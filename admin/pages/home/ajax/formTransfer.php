<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$sql = "SELECT j.tj_name,o.*, m.mem_email
        FROM order_installment o, order_detail od , orders ods, t_job j,t_member m
        WHERE o.status_pay = 'Y'
        and o.od_id = od.od_id
        and od.o_id = ods.o_id
        AND od.mem_id = m.mem_id
        and ods.tj_id = j.tj_id
        and j.tj_status not in ('E')
        and o.installment_id = $id
        order By o.date_update";


$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$row        = $json['data'];

//echo $sql;
$tj_name            = $row[0]['tj_name'];
$img_transfer       = $row[0]['img_transfer'];
$installment_no     = $row[0]['installment_no'];
$transfer_img       = $row[0]['transfer_img'];
$mem_email          = $row[0]['mem_email'];


?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="installment_id" value="<?=$id?>">
<div class="modal-body">
  <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label>Project</label>
          <input type="text" readonly value="<?= $tj_name ?>" class="form-control"  data-smk-msg="&nbsp;" placeholder="" required="">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>งวดที่</label>
          <input type="text" readonly value="<?= $installment_no ?>" class="form-control"  data-smk-msg="&nbsp;" placeholder="" required="">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>จำนวนเงิน</label>
          <input type="text" name="transfer_price" value=""  OnKeyPress="return chkNumber(this)" class="form-control"  data-smk-msg="&nbsp;" placeholder="" required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Email ผู้รับจ้าง</label>
          <input type="text" name="mem_email" value="<?= $mem_email ?>" class="form-control"  data-smk-msg="&nbsp;" placeholder="" required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>หลักฐานการโอนเงิน </label>
          <input name="transfer_img" onchange="readURL(this,'p_img')" type="file" accept="image/*" class="form-control custom-file-input" required>
        </div>
      </div>
      <div class="col-md-6">
        <div id="p_img">
          <?php if($action == 'EDIT'){ ?>
          <img width="100" src="../../../images/slip/<?=$transfer_img?>" onerror="this.onerror='';this.src='../../image/no-image.jpg'">
          <?php } ?>
        </div>
      </div>
  </div>
  <!-- /.row -->
<!-- </div> -->
<!-- </div> -->
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
<button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
