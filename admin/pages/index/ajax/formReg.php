<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$optionProvince = getoptionProvince("");
?>
<input type="hidden" id="action" name="action" value="ADD">
<input type="hidden" id="typeDevice" name="typeDevice" value="WEB">
<div class="modal-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อ</label>
        <input value="" name="member_name" type="text" class="form-control" placeholder="ชื่อ"  data-smk-msg="&nbsp;" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>นามสกุล</label>
        <input value="" name="member_lname" type="text" class="form-control" placeholder="นามสกุล"  data-smk-msg="&nbsp;" required>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>ชื่อหน่วยงาน/สังกัด</label>
        <input value="" name="department_name" type="text" class="form-control" placeholder="ชื่อหน่วยงาน"  data-smk-msg="&nbsp;" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Email</label>
        <input value="" name="email" type="email" class="form-control" placeholder=""  data-smk-msg="&nbsp;"  required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เบอร์โทรศัพท์</label>
        <input value="" name="tel" type="tel" class="form-control" placeholder=""  data-smk-msg="&nbsp;"  required>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>เอกสารของหน่วยงาน (JPG,PNG,PDF ไฟล์)</label>
        <input name="document" type="file" class="form-control custom-file-input" accept="image/jpeg,image/png,application/pdf">
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>วัตถุประสงค์ของการขอใช้งาน</label>
        <input value="" name="objective" type="text" class="form-control" placeholder=""  data-smk-msg="&nbsp;"  required>
      </div>
    </div>
    <div class="col-md-7">
      <div class="form-group">
        <label>ที่อยู่หน่วยงาน</label>
        <input value="" name="address" type="text" class="form-control" placeholder="เลขที่ หมู่ อาคาร"  data-smk-msg="&nbsp;"  required>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>จังหวัด</label>
        <select name="province_id" id="province" onchange="getProvince(this.value,'P')" class="form-control select2" style="width: 100%;" required>
          <option value="">&nbsp;</option>
          <?=$optionProvince;?>
        </select>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>อำเภอ</label>
        <select name="district_id" id="district" onchange="getDistrict(this.value,'D')" class="form-control select2" style="width: 100%;" required>
          <option value="">&nbsp;</option>
        </select>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ตำบล</label>
        <select name="subdistrict_id" id="subdistrict"  onchange="getSubDistrict(this.value,'SD')" class="form-control select2" style="width: 100%;" required>
          <option value="">&nbsp;</option>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>รหัสไปรษณีย์</label>
        <input name="zipcode" id="zipcode" class="form-control" >
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ชื่อผู้ใช้งาน</label>
        <input value="<?=@$user_login?>" onblur="checkUserCode()" id="user_login" name="user_login" type="text" class="form-control" placeholder="User Login" required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Password</label>
        <input value="" name="user_password" id="pass1" type="password" autocomplete="new-password" data-smk-msg="&nbsp;" class="form-control" placeholder="Password" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Confirm Password</label>
        <input value="" name="cfm_user_password" id="pass2" type="password" class="form-control" data-smk-msg="&nbsp;" placeholder="Confirm Password" required>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>รูปโปรไฟล์</label>
        <input name="user_img" type="file" class="form-control custom-file-input" onchange="readURL(this,'showImg');" accept="image/x-png,image/gif,image/jpeg">
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group" id="showImg" style="margin-bottom:0px;margin-top: 15px;">
          <img src="<?= $user_img ?>"  onerror="this.onerror='';this.src='../../image/user.png'" style="height: 60px;">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
  })
</script>
