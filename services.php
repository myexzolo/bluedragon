<style>
        .btn-appStore {
            background-image: url('assets/img/apple-en.png');
            background-size: auto;
            border: 0px;
            width: 200px;
            height: 60px;
            margin-right: 10px;
        }

        .btn-googleplay {
            background-image: url('assets/img/google-en.png');
            background-size: auto;
            border: 0px;
            width: 200px;
            height: 60px;
        }
    </style>
<section id="services" class="services section-bg">
  <div class="container">

    <div class="section-title">
      <h2>วิธีการสั่งซื้อ</h2>
    </div>

    <div class="row">
      <div class="col-lg-6 col-md-12 icon-box">
        <div class="row">
          <div class="col-md-5" style="margin-bottom:20px;" align="center">
            <img src="assets/img/iphone_dealers_rachachok.png" style="width: 90%"/>
          </div>
          <div class="col-md-7">
            <a target="_blank" class="linkBuy" href="<?=$url_dealers;?>">
            <h4>
              สั่งซื้อด้วยตนเอง <img src="images/tap.gif" style="height:30px;" lazy="loaded">
            </h4>

            </a>
            <div style="text-align: justify;line-height:28px;">
              <p>&nbsp;&nbsp;&nbsp;&nbsp;เพียงแค่มีเบอร์มือถือในประเทศไทยสำหรับรับ SMS ลงทะเบียนยืนยันตัวตนในการเข้าใช้งาน คุณก็สามารถสั่งซื้อลอตเตอรี่ออนไลน์ด้วยตัวเองได้ง่ายๆ ไม่ว่าจะอยู่ที่ไหนก็ตาม</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-12 icon-box">
        <div class="row">
          <div class="col-md-5" style="margin-bottom:20px;" align="center">
            <img src="assets/img/iphone_line_rachachok.png" style="width: 90%"/>
          </div>
          <div class="col-md-7">
            <h4><a target="_blank" class="linkBuy" href="https://line.me/R/ti/p/@rachachok">สั่งซื้อผ่านแอดมิน <img src="images/tap.gif" style="height:30px;" lazy="loaded"></a></h4>
            <div style="text-align: justify;line-height:28px;">
              <p>&nbsp;&nbsp;&nbsp;&nbsp;หากคุณอยู่ต่างประเทศ และ ไม่มีเบอร์มือถือที่เปิดในประเทศไทยเพื่อใช้ในการลงทะเบียน คุณสามารถ "แอดไลน์ @rachachok" และแจ้งเลขที่ต้องการกับแอดมิน หลังจากนั้นแอดมินจะค้นหาเลขส่งมาให้คุณได้เลือกซื้ออย่างจุใจ หรือหากคุณไม่ถนัดในการใช้แพลตฟอร์มด้วยตัวเอง ก็สามารถไลน์บอกเลขที่ต้องการกับแอดมินได้เช่นเดียวกัน เพียงไม่กี่นาที คุณก็จะได้เป็นเจ้าของเลขที่ต้องการได้ง่ายๆแล้วหล่ะ</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 col-md-12 icon-box">
        <div class="row">
          <div class="col-md-6" align="center" style="margin-bottom:20px;">
            <div style="margin-bottom:20px;box-shadow: 0 0px 5px rgb(0 0 0 / 20%);border-radius: 8px;background: #ffffff;">
            <img src="assets/img/ohYqXhWUeZHXIVmjin07.png" style="width: 90%"/>
            </div>
          </div>
          <div class="col-md-6">
            <h4>สั่งซื้อผ่าน Application</h4>
            <div style="text-align: justify;line-height:28px;">
              <p>&nbsp;&nbsp;&nbsp;&nbsp;เพียงแค่ดาวโหลดแอปมังกรฟ้า ลงทะเบียนยืนยันตัวตนเข้าใช้งาน กรอกเลขบัญชีที่จะใช้รับเงินเมื่อถูกรางวัล เพียงเท่านี้คุณก็สามารถสั่งซื้อล็อตเตอรี่ได้ง่ายๆ ไม่ว่าคุณจะอยู่ที่ไหนก็ตาม
วีธีนี้สะดวก รวดเร็ว และปลอดภัย</p>
              <button class="btn-appStore" onclick="window.open('https://apps.apple.com/th/app/%E0%B8%A1-%E0%B8%87%E0%B8%81%E0%B8%A3%E0%B8%9F-%E0%B8%B2/id1555211722?l=th', '_blank')"/>
              <button class="btn-googleplay" onclick="window.open('https://play.google.com/store/apps/details?id=kjc.bluedragon.app', '_blank')"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section><!-- End Services Section -->
