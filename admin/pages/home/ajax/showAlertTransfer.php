<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

if(!isset($_SESSION))
{
    session_start();
}
?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>
<div class="box-body">
  <table class="table table-bordered table-striped table-hover" id="tableDisplay2">
    <thead>
      <tr class="text-center">
        <th style="width:40px">ลำดับ</th>
        <th>Project</th>
        <th>Installment</th>
        <th style="width:80px">ชื่อผู้รับจ้าง</th>
        <th style="width:80px">แจ้งโอน</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $sql = "SELECT j.tj_name,o.*, m.mem_email, m.mem_id, m.mem_fname
              FROM order_installment o, order_detail od , orders ods, t_job j, t_member m
              WHERE o.type_pay = 'transfer' and o.status_pay = 'Y' and o.transfer_status = 'P'
              and o.od_id = od.od_id
              and od.o_id = ods.o_id
              AND od.mem_id = m.mem_id
              and ods.tj_id = j.tj_id
              and j.tj_status not in ('E')
              order By o.date_update";

      $query = DbQuery($sql,null);
      $row = json_decode($query,true);


      if($row['dataCount'] > 0){

        foreach ($row['data'] as $key => $value) {
?>
<tr class="text-center">
  <td><?=$key+1;?></td>
  <td align="left"><?=$value['tj_name'] ?></td>
  <td align="center"><?='งวดที่ '.$value['installment_no'];?></td>
  <td align="center"><?= $value['mem_fname']; ?></td>
  <td align="center">
    <a class="btn_point"><i class="fa fa-bell text-navy" onclick="showFormTransfer('<?=$value['installment_id']?>')"></i></a>
  </td>
</tr>
<?php
        }
      }
?>
</tbody>
</table>
<script>
$(function () {
  $('#tableDisplay2').DataTable({
    'paging'      : false,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : false,
    'info'        : false,
    'autoWidth'   : false
  });
})
</script>

</div>
