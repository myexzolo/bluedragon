<?php
    include('../../../inc/function/mainFunc.php');
    include('../../../inc/function/connect.php');

    $action       = $_POST["action"];
    $id           = $_POST["fb_post_id"];
    $message      = "";

    unset($_POST["action"]);

    if(empty($id) && $action == "ADD")
    {
      unset($_POST["fb_post_id"]);
      $_POST['date_create']  =	date("Y/m/d H:i:s");

      $sql = DBInsertPOST($_POST,'t_fb_post');
    }
    else if($action == "EDIT")
    {
      $sql = DBUpdatePOST($_POST,'t_fb_post','fb_post_id');
    }
    else if($action == "DEL")
    {
      $sql = "UPDATE t_fb_post SET is_active = 'D' WHERE fb_post_id = '$id'";
    }


    $query      = DbQuery($sql,null);
    $row        = json_decode($query, true);
    $errorInfo  = $row['errorInfo'];

    if(intval($errorInfo[0]) == 0){
      header("Content-Type: application/json");
      exit(json_encode(array("status" => "success","message" => "success")));
    }else{
      header("Content-Type: application/json");
      exit(json_encode(array("status" => "danger","message" => 'fail')));
    }

  ?>
