<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

if(!isset($_SESSION))
{
    session_start();
}

$date   = date('Y/m/d');

$sql   = "SELECT * FROM t_job j, orders o, order_detail od
          where j.is_acive = 'Y' and j.tj_status not in('E') and j.tj_id = o.tj_id and o.o_id = od.o_id and od.status = 'A'";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$row        = $json['data'];
$dataCount  = $json['dataCount'];

// header('Content-Type: application/json');
// exit(json_encode(array('status' => true,'message' => $dataCount,'count'=> $count)));
?>

<div class="box-body">
<!-- Progress bars -->
<?php
if($dataCount > 0)
{
  for($x=0; $x < $dataCount ; $x++)
  {
      $od_id = $row[$x]['od_id'];

      $sqls   = "SELECT count(installment_id) count ,
                        SUM(case when status_pay = 'Y' then 1 else 0 end) as countPay
                 FROM order_installment where od_id = $od_id ";

      $query      = DbQuery($sqls,null);
      $jsons       = json_decode($query, true);
      $rows        = $jsons['data'];
      $dataCounts  = $jsons['dataCount'];

      $count    = $rows[0]['count'];
      $countPay = $rows[0]['countPay'];

      $per      =  ($countPay/$count) * 100;



?>
    <div class="clearfix">
      <span class="pull-left"><?= @$row[$x]['tj_name'] ?></span>
      <small class="pull-right"><?=$per ?>%</small>
    </div>
    <div class="progress sm">
      <div class="progress-bar progress-bar-green" style="width: <?=$per ?>%;"></div>
    </div>
<?php
  }
}else{
?>

<?php
}
?>
</div>
