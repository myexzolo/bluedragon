<?php
    include('../../../inc/function/mainFunc.php');
    include('../../../inc/function/connect.php');

    $action       = $_POST["action"];
    $id           = $_POST["post_id"];
    $message      = "";

    $post_img    = @$_FILES['post_img'];
    $pathUpload  = "../../../../images/articles/";


    if($post_img['error'] == 0)
    {
      if($action == "ADD"){
        $new_name = "a_".randomString(6,2);
      }else
      {
        $new_name = "a_".$id;
      }
      $_POST["post_img"] = uploadfile($post_img,$pathUpload,$new_name)['image'];
    }
    else if($post_img['error'] == 1)
    {
      header("Content-Type: application/json");
      exit(json_encode(array("status" => "danger","message" => 'The uploaded file exceeds the upload_max_filesize')));
    }
    else{
      unset($_POST["post_img"]);
    }

    unset($_POST["action"]);

    if(empty($id) && $action == "ADD")
    {
      unset($_POST["post_id"]);
      $_POST['date_create']  =	date("Y/m/d H:i:s");

      $sql = DBInsertPOST($_POST,'t_post');
    }
    else if($action == "EDIT")
    {
      $sql = DBUpdatePOST($_POST,'t_post','post_id');
    }
    else if($action == "DEL")
    {
      $sql = "UPDATE t_post SET is_active = 'D' WHERE post_id = '$id'";
    }


    $query      = DbQuery($sql,null);
    $row        = json_decode($query, true);
    $errorInfo  = $row['errorInfo'];

    // echo $sql;
    if(intval($errorInfo[0]) == 0){
      header("Content-Type: application/json");
      exit(json_encode(array("status" => "success","message" => "success")));
    }else{
      header("Content-Type: application/json");
      exit(json_encode(array("status" => "danger","message" => 'fail')));
    }

  ?>
