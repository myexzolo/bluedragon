<?php

$sql   = "SELECT * FROM t_fb_video";

$query      = DbQuery($sql,null);
$row        = json_decode($query, true);

$code_embed = @$row['data'][0]['code_embed'];

?>
<section id="about" class="about faq2">
  <div class="container">
    <div class="row content">
      <div class="col-lg-6">
        <!-- Load Facebook SDK for JavaScript -->
        <?= $code_embed ?>
      </div>
      <div class="col-lg-6 pt-4 pt-lg-0">
        <div class="row d-flex align-items-stretch">
          <div class="col-lg-12 txt-about">
          <i class="bx bxs-quote-alt-left"></i>
          ซื้อลอตเตอรี่กับมังกรฟ้าราชาโชค ลูกค้าจะมีโชคของพระราชา ราชาโชค คือ การได้มาโดยง่าย มีความมั่งคัง ได้เรื่อยๆ สบายๆ ไม่ต้องลงทุนลงแรงมาก มังกรฟ้าราชาโชคขอให้ทุกท่านที่เข้ามาได้เป็นเศรษฐี
          <i class="bx bxs-quote-alt-right"></i>
          </div>
          <div class="col-lg-12 faq-item" style="text-align: justify;text-justify: inter-word;">
            <i class="bx bx-star"></i>
            <h4>มังกรฟ้าคืออะไร ?</h4>
            <p>มังกรฟ้าคือ Platform การจัดระบบการซื้อ-ขายลอตเตอรี่บนโลกออนไลน์โดยใช้ระบบ Ai เข้ามาจัดการให้เป็นระบบระเบียบ เข้าถึงง่าย และสะดวกมากขึ้น เป็นพื้นที่ที่ให้ผู้ซื้อและผู้ขายทั่วโลก ได้มาพบกันเพื่อซื้อขายแลกเปลี่ยนสลากกินแบ่งรัฐบาลบนแพลตฟอร์มของมังกรฟ้า</p>
          </div>
          <div class="col-lg-12 faq-item" style="text-align: justify;text-justify: inter-word;">
            <i class="bx bx-star"></i>
            <h4>ทำไมต้องมังกรฟ้า ?</h4>
            <p>มังกรฟ้าถูกสร้างขึ้นมาเพื่ออำนวยความสะดวกให้แก่ผู้ซื้อที่ชื่นชอบการซื้อลอตเตอรี่ ได้มีตัวเลือกในการซื้อมากขึ้น และเพิ่มความสะดวกที่ไม่ว่าจะอยู่ที่ไหน เมื่อไหร่ ก็สามารถเลือกซื้อเลขที่ต้องการได้ตลอดเวลา ในราคาที่สมเหตุสมผล</p>
          </div>
          <div class="col-lg-12 faq-item" style="text-align: justify;text-justify: inter-word;">
            <i class="bx bx-star"></i>
            <h4>ซื้อมังกรฟ้ายังไง ?</h4>
            <p>การซื้อลอตเตอรี่ออนไลน์ของมังกรฟ้าจะเป็นการซื้อผ่านตัวแทนจำหน่ายเท่านั้นซึ่ง "มังกรฟ้า ราชาโชค" เป็นตัวแทนรุ่นบุกเบิกของมังกรฟ้าที่มียอดขายสูงสุดและยอดลูกค้าถูกรางวัลสูงสุดของ มังกรฟ้า ลอตเตอรี่ ออนไลน์</p>
          </div>
        </div>
      </div>
    </div>

  </div>
</section><!-- End About Section -->
