<!DOCTYPE html>
  <?php
  header("Content-type:text/html; charset=UTF-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);

  $pageName     = "Video Facebook";
  $pageCode     = "";
  ?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ระบบ Back Office - <?= $pageName ?></title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/video_fb.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php
          include("../../inc/sidebar.php");

          $sql   = "SELECT * FROM t_fb_video";

          $query      = DbQuery($sql,null);
          $row        = json_decode($query, true);
          $rows       = $row['data'];


          $code_embed = @$rows[0]['code_embed'];

        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              <?= $pageName ?>
              <small><?=$pageCode ?></small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-home"></i> หน้าหลัก</a></li>
              <li class="active"><?= $pageName ?></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php"); ?>
            <!-- Main row -->
            <!-- <form novalidate action="ajax/AED.php" method="post"> -->
            <form id="formAED" novalidate enctype="multipart/form-data">
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12">
                <div class="box box-warning">
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Embed Video</label>
                          <textarea name="code_embed" id="code_embed" class="form-control" rows="7" required data-smk-msg="&nbsp;"><?=$code_embed ?></textarea>
                          <input value="1" name="id" type="hidden">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box-footer">
                    <button type="submit"  style="width:190px;height:40px;font-size:22px;" class="btn btn-success btn-flat pull-right">
                      <i class="fa fa-save"></i>&nbsp;
                      บันทึก
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/video_fb.js"></script>
    </body>
  </html>
