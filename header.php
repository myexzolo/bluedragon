<header id="header" class="fixed-top d-flex align-items-center header-transparent">
  <div class="container d-flex align-items-center justify-content-between">

    <div class="logo">
      <h1>
        <a href="index.php">
            <img src="assets/img/logo.png" alt="มังกรฟ้า ราชาโชค  ล็อตเตอรี่ ออนไลน์" style="height:100px;">
        </a>
    </h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
    </div>

    <nav id="navbar" class="navbar">
      <ul>
        <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
        <li><a class="nav-link scrollto" href="#about">เกี่ยวกับเรา</a></li>
        <li><a class="nav-link scrollto" href="#blog">Blog</a></li>
        <li><a class="nav-link scrollto" href="#services">วิธีการสั่งซื้อ</a></li>
        <!-- <li><a class="nav-link scrollto " href="#portfolio">คำถามที่พบบ่อย</a></li> -->
        <li><a href="<?=$url_dealers ?>"  target="_blank" class="btn-get-started scrollto" >
            <div class="salelotto">ซื้อล็อตเตอรี่ออนไลน์</div>
          </a>
        </li>
      </ul>
      <i class="bi bi-list mobile-nav-toggle"></i>
    </nav><!-- .navbar -->

  </div>
</header><!-- End Header -->
