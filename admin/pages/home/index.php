<!DOCTYPE html>
<?php
$pageName     = "หน้าหลัก";
$pageCode     = "Home";
?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ระบบ Back Office - <?=$pageName ?> </title>
      <link rel="shortcut icon" type="image/png" href="../../../assets/img/favicon.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/home.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php
          include("../../inc/sidebar.php");
          include('../../inc/function/mainFunc.php');
          $role_list  = $_SESSION['member'][0]['role_list'];
          $user_login = $_SESSION['member'][0]['user_login'];
          $roleArr   = explode(",",$role_list);
          $noDisplayVendor = "";
          $venDorID = "";

          $scol1 = "col-md-5";
          $scol2 = "col-md-4";
          $scol3 = "col-md-3";

          $RoleAdmin = false;
          $roleCodeArr  = explode(",",$_SESSION['ROLE_USER']['role_code']);
          // print_r($_SESSION['member']);
          if (in_array("ADM", $roleCodeArr) || in_array("SADM", $roleCodeArr)|| in_array("PFIT", $roleCodeArr)){
            $RoleAdmin = true;
          }
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>หน้าหลัก</h1>
            <ol class="breadcrumb">
              <li><a href=""><i class="fa fa-home"></i> Home</a></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php");
                $date = date('01/m/Y')." - ".date('t/m/Y');
            ?>
        </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/home.js"></script>
      <script>
          $(".select2").select2();
      </script>
    </body>
  </html>
