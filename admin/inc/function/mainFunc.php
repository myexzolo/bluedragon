<?php

function getUrlHost(){
  $sub = "";
  $http = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http");
  $hosName =  $http."://$_SERVER[HTTP_HOST]/";
  $sub  = $_SERVER['REQUEST_URI'];
  $subs = explode("/", $sub);
  //print_r($subs);
  if(count($subs) > 2 && $http == 'http'){
    $hosName .= $subs[1]."/";
  }

  return $hosName;
}

function addPages($path){
  $SYS_NAME = "ระบบ Back Office";
  $path = $path;
  $rootpath = "../../../pages/$path";
  if (!file_exists("$rootpath/js"))
  {
     mkdir("$rootpath/js", 0777, true);
  }

  if (!file_exists("$rootpath/css"))
  {
     mkdir("$rootpath/css", 0777, true);
  }

  if (!file_exists("$rootpath/ajax"))
  {
     mkdir("$rootpath/ajax", 0777, true);
  }

  $createFileCSS=fopen("$rootpath/css/$path.css",'w');
  $createFileJS=fopen("$rootpath/js/$path.js",'w');
  $createFilePHP=fopen("$rootpath/index.php",'w');
  $textPHP = '<!DOCTYPE html>
  <?php
  $pageName     = "";
  $pageCode     = "";
  ?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>'.$SYS_NAME.' - <?= $pageName ?></title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/'.$path.'.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              <?= $pageName ?>
              <small><?=$pageCode ?></small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-home"></i> หน้าหลัก</a></li>
              <li class="active"><?= $pageName ?></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php"); ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12">

              </div>
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/'.$path.'.js"></script>
    </body>
  </html>
  ';
  fwrite($createFilePHP, $textPHP);
  fclose($createFilePHP);
}


function convert($number){ //inputString  example require .00
  $txtnum1 = array('ศูนย์','หนึ่ง','สอง','สาม','สี่','ห้า','หก','เจ็ด','แปด','เก้า','สิบ');
  $txtnum2 = array('','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน');
  $number = str_replace(",","",$number);
  $number = str_replace(" ","",$number);
  $number = str_replace("บาท","",$number);
  $number = explode(".",$number);
  if(sizeof($number)>2){
    $number = number_format($number, 2);
  }

  $strlen = strlen($number[0]);
  $convert = '';
  for($i=0;$i<$strlen;$i++){
  	$n = substr($number[0], $i,1);
  	if($n!=0){
  		if($i==($strlen-1) AND $n==1){ $convert .= 'เอ็ด'; }
  		elseif($i==($strlen-2) AND $n==2){  $convert .= 'ยี่'; }
  		elseif($i==($strlen-2) AND $n==1){ $convert .= ''; }
  		else{ $convert .= $txtnum1[$n]; }
  		$convert .= $txtnum2[$strlen-$i-1];
  	}
  }
  $convert .= 'บาท';
  if($number[1]=='0' || $number[1]=='00' || $number[1]==''){
  $convert .= 'ถ้วน';
  }else{
  $strlen = strlen($number[1]);
  for($i=0;$i<$strlen;$i++){
  $n = substr($number[1], $i,1);
  	if($n!=0){
  	if($i==($strlen-1) AND $n==1){$convert
  	.= 'เอ็ด';}
  	elseif($i==($strlen-2) AND
  	$n==2){$convert .= 'ยี่';}
  	elseif($i==($strlen-2) AND
  	$n==1){$convert .= '';}
  	else{ $convert .= $txtnum1[$n];}
  	$convert .= $txtnum2[$strlen-$i-1];
  	}
  }
  $convert .= 'สตางค์';
  }
  return $convert;
}


function getDateOracle($str){
  if($str != "")
  {
        $str = " TO_CHAR($str,'YYYY-MM-DD') as $str ";
  }
  return $str;
}

function getQueryDate($str){
  if($str != "")
  {
        $str = " TO_CHAR($str,'YYYY-MM-DD') as $str ";
  }
  return $str;
}

function getQueryDateWere($str){
  if($str != ""){
        $str = " TO_CHAR($str,'YYYY-MM-DD') ";
  }
  return $str;
}

function getQueryDateOracle($str, $format){
  return $str = " TO_CHAR($str,'$format') as $str ";
}



function queryDateOracle($formatDate){
  if($formatDate != ""){
       $formatDate = "TO_DATE('$formatDate','YYYY-MM-DD')";
  }
  return $formatDate;
}
function queryDate($formatDate){
  if($formatDate != ""){
       $formatDate = "TO_DATE('$formatDate','YYYY-MM-DD')";
  }
  return $formatDate;
}

function queryDateTimeOracle($formatDate){
  if($formatDate != ""){
       $formatDate = "TO_DATE('$formatDate','YYYY-MM-DD HH24:MI:SS')";
  }
  return $formatDate;
}


function removePage($dir) {
  if (is_dir($dir)) {
    $objects = scandir($dir);
    foreach ($objects as $object) {
      if ($object != "." && $object != "..") {
        if (filetype($dir."/".$object) == "dir")
           removePage($dir."/".$object);
        else unlink   ($dir."/".$object);
      }
    }
    reset($objects);
    rmdir($dir);
  }
 }


 function getRealIP()
    {
      $ipaddress = '';
      if (getenv('HTTP_CLIENT_IP'))
          $ipaddress = getenv('HTTP_CLIENT_IP');
      else if(getenv('HTTP_X_FORWARDED_FOR'))
          $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
      else if(getenv('HTTP_X_FORWARDED'))
          $ipaddress = getenv('HTTP_X_FORWARDED');
      else if(getenv('HTTP_FORWARDED_FOR'))
          $ipaddress = getenv('HTTP_FORWARDED_FOR');
      else if(getenv('HTTP_FORWARDED'))
         $ipaddress = getenv('HTTP_FORWARDED');
      else if(getenv('REMOTE_ADDR'))
          $ipaddress = getenv('REMOTE_ADDR');
      else
          $ipaddress = 'UNKNOWN';
      return $ipaddress;
    }

function randomString($length = 4 , $type = 0) {
  // type 0 = UPPER STRING , 1 = LOWER STRING , 2 = MATCH ,
  // 3 = UPPER & LOWER STRING , 4 UPPER & LOWER & MATCH
  if($type == 0){
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  }else if($type == 1){
    $characters = 'abcdefghijklmnopqrstuvwxyz';
  }else if($type == 2){
    $characters = '0123456789';
  }else if($type == 3){
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  }else if($type == 4){
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  }else if($type == 5){
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/.,!@#$%^&*()-_+=';
  }
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
}

function getOTP(){
  return  array('REF' => randomString($length = 5 , $type = 0) , 'OTP' => randomString($length = 4 , $type = 2) );
}

function DateTimeThai($strDate){
    $dateStr = "";
    if($strDate != null && $strDate != ""){
      $strYear = date("Y",strtotime($strDate))+543;
  		$strMonth= date("m",strtotime($strDate));
  		$strDay= date("d",strtotime($strDate));
  		$strHour= date("H",strtotime($strDate));
  		$strMinute= date("i",strtotime($strDate));
  		$strSeconds= date("s",strtotime($strDate));

  		$dateStr = "$strDay/$strMonth/$strYear $strHour:$strMinute:$strSeconds";
    }
    return $dateStr;
}

function DateThai($strDate){
  $dateStr = "";
  if($strDate != null && $strDate != ""){
    $strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("m",strtotime($strDate));
		$strDay= date("d",strtotime($strDate));

    $dateStr = "$strDay/$strMonth/$strYear";
  }
  return $dateStr;

}

function DateEn($strDate){
		$strYear = date("Y",strtotime($strDate));
		$strMonth= date("m",strtotime($strDate));
		$strDay= date("d",strtotime($strDate));

		return "$strDay/$strMonth/$strYear";
}

function dateThToEn($date,$format,$delimiter)
{
    $formatLowerCase  = strtolower($format);//var formatLowerCase=format.toLowerCase();
    $formatItems      = explode($delimiter,$formatLowerCase);//var formatItems=formatLowerCase.split(delimiter);
    $dateItems        = explode($delimiter,$date);//var dateItems=date.split(delimiter);
    $monthIndex       = array_search("mm",$formatItems);//var monthIndex=formatItems.indexOf("mm");
    $dayIndex         = array_search("dd",$formatItems);//var dayIndex=formatItems.indexOf("dd");
    $yearIndex        = array_search("yyyy",$formatItems);//var yearIndex=formatItems.indexOf("yyyy");
    $month            = $dateItems[$monthIndex];//var month=parseInt(dateItems[monthIndex]);

    $yearth = $dateItems[$yearIndex];
    if( $yearth > 2450){
      $yearth -= 543;
    }
    $dateEN = $yearth."-".sprintf("%02d", $month)."-".sprintf("%02d", $dateItems[$dayIndex]);
    return $dateEN;
}


function datetimeEn($date,$format,$delimiter)
{
    $formatLowerCase  = strtolower($format);//var formatLowerCase=format.toLowerCase();
    $formatDateTime   = explode(" ",$date);//var formatItems=formatLowerCase.split(delimiter);
    $date             = $formatDateTime[0];
    $time             = $formatDateTime[1];

    $formatItems      = explode($delimiter,$formatLowerCase);//var formatItems=formatLowerCase.split(delimiter);
    $dateItems        = explode($delimiter,$date);//var dateItems=date.split(delimiter);
    $monthIndex       = array_search("mm",$formatItems);//var monthIndex=formatItems.indexOf("mm");
    $dayIndex         = array_search("dd",$formatItems);//var dayIndex=formatItems.indexOf("dd");
    $yearIndex        = array_search("yyyy",$formatItems);//var yearIndex=formatItems.indexOf("yyyy");
    $month            = $dateItems[$monthIndex];//var month=parseInt(dateItems[monthIndex]);

    $yearth = $dateItems[$yearIndex];
    if( $yearth > 2450){
      $yearth -= 543;
    }
    $dateEN = $yearth."-".sprintf("%02d", $month)."-".sprintf("%02d", $dateItems[$dayIndex])." ".$time;
    return $dateEN;
}

function mailsend($arr){

  $sql = "SELECT * FROM mail_sent";
  $query = DbQuery($sql,null);
  $row   = json_decode($query, true);
  $title = $arr['title'];

  $strmail = '<!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title></title>
      </head>
      <body>
        Username = '.$arr['username'].' <br />
        Password = '.$arr['password'].'
      </body>
    </html>
    ';

  $mail             = new PHPMailer(true);
  $mail->isSMTP();
  $mail->CharSet    = "utf-8";
  $mail->Host       = $row['data'][0]['mail_host'];
  $mail->Port       = $row['data'][0]['mail_port'];
  $mail->SMTPSecure = $row['data'][0]['mail_smtpsecure'];
  $mail->SMTPAuth   = true;
  $mail->Username   = $row['data'][0]['mail_username'];
  $mail->Password   = $row['data'][0]['mail_password'];
  $mail->SetFrom($row['data'][0]['mail_username'], $title);
  $mail->addAddress($arr['email'], $arr['email']);
  //$mail->SMTPDebug  = 3;
  //$mail->Debugoutput = function($str, $level) {echo "debug level $level; message: $str";}; //$mail->Debugoutput = 'echo';
  $mail->IsHTML(true);

  $mail->Subject = $title;
  $mail->Body    = $strmail;
  // $mail->addAttachment('../image/Preloader_3.gif', 'Preloader_3.gif');

  if($mail->send()) { return 200; } else { return 404; }
  // $mail->send()?return 200;:return 404;

}

function mailsendMail($arr){

  $sql = "SELECT * FROM mail_sent";
  $query = DbQuery($sql,null);
  $row   = json_decode($query, true);
  $title = $arr['title'];

  $strmail = '<!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title></title>
      </head>
      <body>
        '.$arr['message'].'
      </body>
    </html>
    ';
  if(isset($arr['headerEmail']))
  {
    $headerEmail = $arr['headerEmail'];
  }
  else{
    $headerEmail = $arr['email'];
  }
  $mail             = new PHPMailer(true);
  $mail->isSMTP();
  $mail->CharSet    = "utf-8";
  $mail->Host       = $row['data'][0]['mail_host'];
  $mail->Port       = $row['data'][0]['mail_port'];
  $mail->SMTPSecure = $row['data'][0]['mail_SMTPSecure'];
  $mail->SMTPAuth   = true;
  $mail->Username   = $row['data'][0]['mail_Username'];
  $mail->Password   = $row['data'][0]['mail_Password'];
  $mail->FromName   = $row['data'][0]['mail_FromName'];
  $mail->SetFrom($row['data'][0]['mail_Username'], $headerEmail);
  $mail->addAddress($arr['email'], $arr['email']);
  //$mail->SMTPDebug  = 3;
  //$mail->Debugoutput = function($str, $level) {echo "debug level $level; message: $str";}; //$mail->Debugoutput = 'echo';
  $mail->IsHTML(true);

  $mail->Subject = $title;
  $mail->Body    = $strmail;

  if(isset($arr['attachment'])){
    $mail->addAttachment($arr['attachment']['path'], $arr['attachment']['newname']);
  }

  if($mail->send()) { return 200; } else { return 404; }
  // $mail->send()?return 200;:return 404;

}

function getDataSSL($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function FIX_PHP_CORSS_ORIGIN(){

 //http://stackoverflow.com/questions/18382740/cors-not-working-php
 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }

}

function getMunu($arrPage)
{
  $sqlp = "SELECT p.*,m.root_id,m.module_name,m.module_icon,r.root_name
           FROM t_page p , t_module m , t_root r
           WHERE p.page_id IN ($arrPage) AND p.is_active = 'Y'
           AND m.module_type = '1' AND m.is_active = 'Y' AND p.module_id = m.module_id
           AND m.root_id = r.root_id
           order by r.root_seq, m.module_order, cast(p.page_seq as unsigned)";
  $queryp = DbQuery($sqlp,null);
  $rows   = json_decode($queryp, true);
  $rowp   = $rows['data'];

  return $rowp;

}




function updateDataLog($arr){
  // ut_event = INS , UPD , DEL
  // ut_topic = table DB
  $sql  = "INSERT INTO uplate_time VALUES(null,now(),'{$arr['ut_event']}','{$arr['ut_topic']}','{$arr['user_id']}')";
  $query = DbQuery($sql,null);
  $row  = json_decode($query, true);
  $num  = $row['dataCount'];

  if(intval($row['errorInfo'][0]) == 0){
    return 200;
  }else{
    return 400;
  }
}

function showUpdateLog($event,$topic){
  $sql = "SELECT * FROM uplate_time WHERE ut_event = '$event' AND ut_topic = '$topic' ORDER BY ut_timestamp DESC";
  $query = DbQuery($sql,null);
  // $row   = json_decode($query, true);
  return $query;
}



function getDataMaster($dataGroup,$datacode){
    $dataDesc = "";
    $sql   = "SELECT * FROM data_master where DATA_GROUP = '$dataGroup' and DATA_CODE = '$datacode' ";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    for ($i=0; $i < $dataCount; $i++) {
      $dataDesc = $json['data'][$i]['DATA_DESC1'];
    }
    return $dataDesc;
}


function getVendorByUserId($userId)
{
    $vendor = "";
    $sql   = "SELECT * FROM t_vendor where is_active = 'Y' and user_id = '$userId'";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    return $vendor;
}

function getoptionDataMaster($dataGroup,$datacode){
    $optionMaster = "";

    $sql   = "SELECT * FROM data_master where DATA_GROUP = '$dataGroup'";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    for ($i=0; $i < $dataCount; $i++) {
      $code         = $json['data'][$i]['DATA_CODE'];
      $dataDesc     = $json['data'][$i]['DATA_DESC1'];
      $selected =  ' ';
      if($datacode == $code && $datacode != ""){
        $selected =  'selected="selected"';
      }
      $optionMaster .= '<option value="'.$code.'" '.$selected.'>'.$dataDesc.'</option>';
    }
    return $optionMaster;
}

function getoptionBranch($code){
    $optionBranch = "";
    $sql   = "SELECT * FROM t_branch where is_active = 'Y' order by branch_code";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    for ($i=0; $i < $dataCount; $i++) {
      $branch_code    = $json['data'][$i]['branch_code'];
      $cname          = $json['data'][$i]['cname'];
      $selected =  ' ';
      if($code == $branch_code && $code != ""){
        $selected =  'selected="selected"';
      }
      $optionBranch .= '<option value="'.$branch_code.'" '.$selected.'>'.$cname.'</option>';
    }
    return $optionBranch;
}

function getoptionVendor($id){
    $option = "";
    $sql   = "SELECT * FROM t_vendor where is_active = 'Y' order by vendor_code";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    for ($i=0; $i < $dataCount; $i++) {
      $vendor_id    = $json['data'][$i]['vendor_id'];
      $vendor_name  = $json['data'][$i]['vendor_name'];
      $selected =  ' ';
      if($id == $vendor_id && $id != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$vendor_id.'" '.$selected.'>'.$vendor_name.'</option>';
    }
    return $option;
}

function getoptionVendorCode($code){
    $option = "";
    $sql   = "SELECT * FROM t_vendor where is_active = 'Y' order by vendor_code";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    for ($i=0; $i < $dataCount; $i++) {
      $vendor_code    = $json['data'][$i]['vendor_code'];
      $vendor_name  = $json['data'][$i]['vendor_name'];
      $selected =  ' ';
      if($code == $vendor_code && $code != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$vendor_code.'" '.$selected.'>'.$vendor_name.'</option>';
    }
    return $option;
}


function getBranchName($code){
    $optionBranch = "";
    $sql   = "SELECT * FROM t_branch where branch_code = '$code'";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];
    $cname = "";
    if($dataCount > 0){
      $cname  = $json['data'][0]['cname'];
    }

    return $cname;
}


function getoptionDepartment($id){
    $option = "";
    $sql   = "SELECT * FROM t_department where is_active = 'Y' order by department_code";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    for ($i=0; $i < $dataCount; $i++) {
      $department_id  = $json['data'][$i]['department_id'];
      $name           = $json['data'][$i]['department_name'];

      $selected =  ' ';
      if($id == $department_id && $id != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$department_id.'" '.$selected.'>'.$name.'</option>';
    }
    return $option;
}


function getoptionProvince($id){
    $option = "";
    $sql   = "SELECT * FROM province order by PROVINCE_NAME";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    //print_r($json);

    for ($i=0; $i < $dataCount; $i++) {
      $province_id    = $json['data'][$i]['province_id'];
      $province_name  = $json['data'][$i]['province_name'];

      $selected =  ' ';
      if($id == $province_id && $id != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$province_id.'" '.$selected.'>'.str_replace("จังหวัด","",$province_name).'</option>';
    }
    return $option;
}

function getProvinceName($id)
{
  $name = "";
  $sql   = "SELECT * FROM province where province_id = '$id'";
  $query = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $dataCount  = $json['dataCount'];
  if($dataCount > 0){
    $name = $json['data'][0]['province_name'];
  }
  return $name;
}

function getoptionDistrict($provinceId,$id){
    $option = "";

    $sql   = "SELECT * FROM districts where province_id = '$provinceId' order by name_th";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    $vowels = array("อำเภอ", "เขต");

    for ($i=0; $i < $dataCount; $i++) {
      $district_id    = $json['data'][$i]['id'];
      $district_name  = $json['data'][$i]['name_th'];

      $selected =  ' ';
      if($id == $district_id && $id != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$district_id.'" '.$selected.'>'.str_replace($vowels,"",$district_name).'</option>';
    }
    return $option;
}

function getDistrictName($id)
{
  $name = "";
  $sql   = "SELECT * FROM districts where id = '$id'";
  $query = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $dataCount  = $json['dataCount'];
  if($dataCount > 0){
    $name = $json['data'][0]['name_th'];
  }
  return $name;
}

function getoptionSubDistrict($districtId,$id){
    $option = "";

    $sql   = "SELECT * FROM subdistricts where districts_id = '$districtId' order by name_th";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    $vowels = array("ตำบล", "แขวง");

    for ($i=0; $i < $dataCount; $i++) {
      $subdistrict_id    = $json['data'][$i]['id'];
      $subdistrict_name  = $json['data'][$i]['name_th'];

      $selected =  ' ';
      if($subdistrict_id == $id && $id != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$subdistrict_id.'" '.$selected.'>'.str_replace($vowels,"",$subdistrict_name).'</option>';
    }
    return $option;
}


function getoptionMember($id)
{
  $option = "";

  $sql   = "SELECT *
            FROM t_user u
            LEFT JOIN pfit_t_member pm ON u.user_id = pm.user_id
            where u.is_active  = 'Y' and  u.role_list not like '0,%' and  u.role_list not like ',0,%'
            and u.role_list != '1'  and u.role_list != ',1'  and  u.role_list  not like '1,%' and  u.role_list not like ',1,%'
            and u.role_list != '2'  and u.role_list != ',2'  and  u.role_list not like '2,%' and  u.role_list not like ',2,%'
            and u.role_list != '5'  and u.role_list != ',5'  and  u.role_list not like '5,%' and  u.role_list not like ',5,%'
            ORDER BY u.user_login,pm.department_name";

  $query = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $dataCount  = $json['dataCount'];

  $vowels = array("ตำบล", "แขวง");

  for ($i=0; $i < $dataCount; $i++) {
    $user_id     = $json['data'][$i]['user_id'];
    $user_login  = $json['data'][$i]['user_login'];
    $department_name  = empty($json['data'][$i]['department_name'])?"":":".$json['data'][$i]['department_name'];

    $selected =  ' ';
    if($id == $user_id && $id != ""){
      $selected =  'selected="selected"';
    }
    $option .= '<option value="'.$user_login.'" '.$selected.'>'.$user_login.$department_name.'</option>';
  }
  return $option;
}


function monthThai($month){
  $arr = array('','ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');
  return $arr[$month];
}

function monthThaiFull($month){
  $arr = array('','มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
  return $arr[$month];
}

function convDatetoThaiMonth($date){
  $dateStr = "";
  if($date != ""){
    $day   = date('d',strtotime($date));
    $month = monthThai(date('n',strtotime($date)));
    $year  = substr(date("Y", strtotime($date))+543,2,2);
    $dateStr = "$day $month $year";
  }
  return $dateStr;
}

function getRoleName($roleList){
    $RoleName= "-";
    $sql   = "SELECT * FROM t_role where role_id in ($roleList) ";
    $query = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $dataCount  = $json['dataCount'];

    for ($i=0; $i < $dataCount; $i++) {
      if($i == 0){
        $RoleName = $json['data'][$i]['role_name'];
      }else{
        $RoleName .= ", ".$json['data'][$i]['role_name'];
      }
    }
    return $RoleName;
}


function resizeImageToUpload($obj,$h,$w,$path,$title) {
  $new_images = "";
  if(isset($obj) && !empty($obj["name"]))
  {
    if(getimagesize($obj['tmp_name']))
    {
      $ext = strtolower(pathinfo($obj["name"], PATHINFO_EXTENSION));
      if($ext == 'gif' || $ext !== 'png' || $ext !== 'jpg' )
      {
        if(!empty($h) || !empty($w))
        {
          $filePath       = $obj['tmp_name'];
          $image          = addslashes($filePath);
          $name           = addslashes($obj['name']);
          $new_images     = $title;

          $x  = 0;
          $y  = 0;

          list($width_orig, $height_orig) = getimagesize($filePath);

          if(empty($h)){
              if($width_orig > $w){
                $new_height  = $height_orig*($w/$width_orig);
                $new_width   = $w;
              }else{
                $new_height  = $height_orig;
                $new_width   = $width_orig;
              }
          }
          else if(empty($w))
          {
              if($height_orig > $h){
                $new_height  = $h;
                $new_width   = $width_orig*($h/$height_orig);
              }else{
                $new_height  = $height_orig;
                $new_width   = $width_orig;
              }
          }
          else
          {
            if($height_orig > $width_orig)
            {
              $new_height  = $height_orig*($w/$width_orig);
              $new_width   = $w;
            }else{
              $new_height  = $h;
              $new_width   = $width_orig*($h/$height_orig);
            }
          }

          $create_width   =  $new_width;
          $create_height  =  $new_height;


          if(!empty($h) && $new_height > $h){
             $create_height = $h;
          }

          if(!empty($w) && $new_width > $w){
            $create_width = $w;
          }

          $imageOrig;
          $imageResize    = imagecreatetruecolor($create_width, $create_height);
          $background     = imagecolorallocatealpha($imageResize, 255, 255, 255, 127);
          imagecolortransparent($imageResize, $background);
          imagealphablending($imageResize, false);
          imagesavealpha($imageResize, true);

          if($ext == 'png'){
            $imageOrig      = imagecreatefrompng($filePath);
            $new_images     = $new_images.".png";

            $pathfile  =  $path."/".$new_images;
            if(file_exists($pathfile))
            {
                unlink($pathfile);
            }

            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagepng($imageResize,$path.$new_images);
          }else if ($ext == 'jpg'){
            $imageOrig      = imagecreatefromjpeg($filePath);
            $new_images     = $new_images.".jpg";

            $pathfile  =  $path."/".$new_images;
            if(file_exists($pathfile))
            {
                unlink($pathfile);
            }

            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagejpeg($imageResize,$path.$new_images);
          }else if ($ext == 'gif'){
            $imageOrig      = imagecreatefromgif($filePath);
            $new_images     = $new_images.".gif";

            $pathfile  =  $path."/".$new_images;
            if(file_exists($pathfile))
            {
                unlink($pathfile);
            }

            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagegif($imageResize,"$path".$new_images);
          }

          imagedestroy($imageOrig);
          imagedestroy($imageResize);
        }else{
          $fileinfo = pathinfo($obj['name']);
          $filetype = strtolower($fileinfo['extension']);
          $new_images = $title.time().".$filetype";
          move_uploaded_file($obj['tmp_name'],$path.'/'.$new_images);
        }
      }
    }
  }
  return $new_images;
}

function Base64ToImage($base64_string,$path,$name) {

    $data = explode( ',', $base64_string );
    $num = 0;
    $type = "png";
    if(count($data) > 0){
        $num = 1;
        switch ($data[0])
        {
          case "data:image/jpeg;base64":
              $type = "jpeg";
              break;
          case "data:image/png;base64":
              $type = "png";
              break;
          default://should write cases for more images types
              $type = "jpg";
              break;
        }
    }

    $name = $name.".".$type;

    $output_file = $path.$name;
    $ifp = fopen( $output_file, 'wb' );

    fwrite( $ifp, base64_decode( $data[$num] ) );
    fclose( $ifp );

    return $name;
}


function resizeImageToBase64($obj,$h,$w,$quality,$user_id_update,$path) {
  $img = "";
  if(isset($obj) && !empty($obj["name"]))
  {

    if(getimagesize($obj['tmp_name']))
    {
      $ext = strtolower(pathinfo($obj["name"], PATHINFO_EXTENSION));
      if($ext == 'gif' || $ext !== 'png' || $ext !== 'jpg' )
      {
        if(!empty($h) || !empty($w))
        {
          $filePath       = $obj['tmp_name'];
          $image          = addslashes($filePath);
          $name           = addslashes($obj['name']);
          $new_images     = "thumbnails_".$user_id_update;

          $x  = 0;
          $y  = 0;

          list($width_orig, $height_orig) = getimagesize($filePath);

          if(empty($h)){
              if($width_orig > $w){
                $new_height  = $height_orig*($w/$width_orig);
                $new_width   = $w;
              }else{
                $new_height  = $height_orig;
                $new_width   = $width_orig;
              }
          }
          else if(empty($w))
          {
              if($height_orig > $h){
                $new_height  = $h;
                $new_width   = $width_orig*($h/$height_orig);
              }else{
                $new_height  = $height_orig;
                $new_width   = $width_orig;
              }
          }
          else
          {
            if($height_orig > $width_orig)
            {
              $new_height  = $height_orig*($w/$width_orig);
              $new_width   = $w;
            }else{
              $new_height  = $h;
              $new_width   = $width_orig*($h/$height_orig);
            }
          }

          $create_width   =  $new_width;
          $create_height  =  $new_height;


          if(!empty($h) && $new_height > $h){
             $create_height = $h;
          }

          if(!empty($w) && $new_width > $w){
            $create_width = $w;
          }


          $imageOrig;
          $imageResize    = imagecreatetruecolor($create_width, $create_height);
          $background     = imagecolorallocatealpha($imageResize, 255, 255, 255, 127);
          imagecolortransparent($imageResize, $background);
          imagealphablending($imageResize, false);
          imagesavealpha($imageResize, true);

          if($ext == 'png'){
            $imageOrig      = imagecreatefrompng($filePath);
            $new_images     = $new_images.".png";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagepng($imageResize,$path.$new_images);
          }else if ($ext == 'jpg'){
            $imageOrig      = imagecreatefromjpeg($filePath);
            $new_images     = $new_images.".jpg";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagejpeg($imageResize,$path.$new_images);
          }else if ($ext == 'gif'){
            $imageOrig      = imagecreatefromgif($filePath);
            $new_images     = $new_images.".gif";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagegif($imageResize,"$path".$new_images);
          }

          imagedestroy($imageOrig);
          imagedestroy($imageResize);

          $image   = file_get_contents($path.$new_images);
          $img     = 'data:image/png;base64,'.base64_encode($image);
        }else{
          $image   = file_get_contents($path.$name);
          $img     = 'data:image/png;base64,'.base64_encode($image);
        }

      }
    }
  }
  return $img;
}


function formatDateThtoEn($date){
  $Datestr = "";
  if(!empty($date) && strlen($date) >= 8){
    $date = str_replace ('-','/',$date);
    $dt = explode("/", $date);
    $d  = $dt[0];
    $m  = $dt[1];
    $y  = $dt[2];

    if($y > 2500){
      $y -= 543;
    }
    if(strlen($d) == 1){
      $d  = "0".$d;
    }

    if(strlen($m) == 1){
      $m  = "0".$m;
    }

    $date = $y."/".$m."/".$d;
    if(checkdate($m,$d,$y)){
      $Datestr = date("Y-m-d", strtotime($date));
    }
  }
  //echo $Datestr."<br>";
  return $Datestr;
}

function formatDateTh($date){
  $Datestr = "";
  if(!empty($date)){
    $d = strtotime($date);
    $Datestr = date("d/m/Y", $d);
  }
  return $Datestr;
}

function yearBirth($date){
  $d1 = new DateTime(date('Y-m-d'));
  $d2 = new DateTime($date);
  $diff = $d2->diff($d1);
  return $diff->y;
}

function yearBirth2($date,$date2){
  // $d1 = new DateTime($date2);
  // $d2 = new DateTime($date);
  // $diff = $d2->diff($d1);
  // return $diff->y;

  ///คำนวนอายุ = ปี พ.ศ. ของวันที่ทำการทดสอบ(อิงตามวันสิ้นสุดขอกิจกรรม)  - ปีพ.ศ.ของวันที่เกิด
  $dob = date('Y', strtotime($date));
  $d2  = date('Y', strtotime($date2));
  $diff = intval($d2) - intval($dob);
  return $diff;
}

function DateDiff($strDate1,$strDate2)
{
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
}

function TimeDiff($strTime1,$strTime2)
{
		return (strtotime($strTime2) - strtotime($strTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}

function DateTimeDiff($strDateTime1,$strDateTime2)
{
		return (strtotime($strDateTime2) - strtotime($strDateTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}

function chkNum($obj){
  $obj = str_replace(",","",$obj);
  if(!isset($obj) || empty($obj)){
      $obj = 0;
  }
  if(!is_numeric($obj)){
    $obj = "0";
  }
  return $obj;
}

function mailsendAttachment($title,$message,$addBCC,$addAttachment){

  $sql = "SELECT * FROM mail_sent";
  $query = DbQuery($sql,null);
  $row   = json_decode($query, true)['data'];
  // print_r($row);
  $strmail = '<!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title></title>
      </head>
      <body>
      '.$message.'
      </body>
    </html>
    ';

  $mail             = new PHPMailer(true);
  $mail->isSMTP();
  $mail->CharSet    = "utf-8";
  $mail->Host       = $row[0]['mail_host'];
  $mail->Port       = $row[0]['mail_port'];
  $mail->SMTPSecure = $row[0]['mail_SMTPSecure'];
  $mail->SMTPAuth   = true;
  $mail->Username   = $row[0]['mail_Username'];
  $mail->Password   = $row[0]['mail_Password'];
  $mail->SetFrom($row[0]['mail_Username'], $title);
  foreach ($addBCC as $value) {
    $mail->addBCC($value, 'ToEmail');
  }
  $mail->IsHTML(true);

  $mail->Subject = $title;
  $mail->Body    = $strmail;
  $mail->addAttachment($addAttachment['path'], $addAttachment['newname']);

  if($mail->send()) { return 200; } else { return 404; }

}

function GET_LINE_TOKEN($token){
  return $token != "" ?$token:'VNOk5CTKAopXH7Ey29JSmOjrJk1wRmwmsNNG8wsAE34';
}

function LINE_NOTIFY_MESSAGE($message,$imageFile,$token){
  $LINE_TOKEN = $token;
  define('LINE_API',"https://notify-api.line.me/api/notify");
  define('LINE_TOKEN', $LINE_TOKEN);

  $queryData = array(
    'message' => $message,
    'imageThumbnail' => $imageFile,
    'imageFullsize' => $imageFile
  );
    $queryData = http_build_query($queryData,'','&');
    $headerOptions = array(
     'http'=>array(
       'method'=>'POST',
       'header'=> "Content-Type: application/x-www-form-urlencoded\r\n"
       ."Authorization: Bearer ".LINE_TOKEN."\r\n"
       ."Content-Length: ".strlen($queryData)."\r\n",
       'content' => $queryData
     ),
     'ssl'=>array(
       "verify_peer"=>false,
       "verify_peer_name"=>false,
      )
    );
    $context = stream_context_create($headerOptions);
    $result = file_get_contents(LINE_API,FALSE,$context);
    $res = json_decode($result,true);
    return $res;
}

function uploadfileImage($attach,$url,$title){
  $fileinfo = pathinfo($attach['name']);
  $filetype = strtolower($fileinfo['extension']);
  $image = $title.time().".$filetype";
  move_uploaded_file($attach['tmp_name'],$url.'/'.$image);

  return array('image' => $image);
}

function uploadfile($attach,$url,$title){
  $fileinfo = pathinfo($attach['name']);
  $filetype = strtolower($fileinfo['extension']);
  $image = $title.time().".$filetype";
  move_uploaded_file($attach['tmp_name'],$url.'/'.$image);

  return array('image' => $image);
}

function uploadfileV2($attach,$url,$title){
  $fileinfo = pathinfo($attach['name']);
  $filetype = strtolower($fileinfo['extension']);
  $image = $title.".$filetype";
  move_uploaded_file($attach['tmp_name'],$url.'/'.$image);

  return array('image' => $image);
}

function uploadfileMulti($attach,$url,$title){
  $arr = array();
  foreach($attach['tmp_name'] as $key => $val){
      $fileinfo = pathinfo($attach['name'][$key]);
      $filetype = strtolower($fileinfo['extension']);
  		$file_tmp =$attach['tmp_name'][$key];
      $image = $title.$key.time().".$filetype";
  		move_uploaded_file($file_tmp, $url.'/'.$image);
      $arr[] = $image;
  }
  $str = implode("|",$arr);
  return array('image' => $str);
}



function mailsendNotification($arr,$bcc){

  $sql = "SELECT * FROM mail_sent";
  $query = DbQuery($sql,null);
  $row   = json_decode($query, true);
  $title = $arr['title'];

  $strmail = '<!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title></title>
      </head>
      <body>
        '.$arr['message'].'
      </body>
    </html>
    ';

  $mail             = new PHPMailer(true);
  $mail->isSMTP();
  $mail->CharSet    = "utf-8";
  $mail->Host       = $row['data'][0]['mail_host'];
  $mail->Port       = $row['data'][0]['mail_port'];
  $mail->SMTPSecure = $row['data'][0]['mail_SMTPSecure'];
  $mail->SMTPAuth   = true;
  $mail->Username   = $row['data'][0]['mail_Username'];
  $mail->Password   = $row['data'][0]['mail_Password'];
  $mail->SetFrom($row['data'][0]['mail_Username'], $title);
  // $mail->addAddress($arr['email'], $arr['email']);
  $mail->IsHTML(true);

  $mail->Subject = $title;
  $mail->Body    = $strmail;

  foreach ($bcc as $value) {
    $mail->addBCC($value, $value);
  }

  if($mail->send()) { return 200; } else { return 404; }

}




?>
