<footer id="footer">
  <div class="footer-top">

    <div class="container">

      <div class="row  justify-content-center">
        <div class="col-lg-6">
          <h3>มังกรฟ้า ราชาโชค</h3>
          <!-- <p>Et aut eum quis fuga eos sunt ipsa nihil. Labore corporis magni eligendi fuga maxime saepe commodi placeat.</p> -->
        </div>
      </div>

      <div class="social-links">
        <!-- <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a> -->
        <a target="_blank" href="https://www.facebook.com/mongkornfahrachachok/" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a target="_blank" href="https://line.me/R/ti/p/@rachachok" class="line"><img style="margin-top: -13px;" src="assets/img/line.png" width="40"/></a>
        <a href="tel: 0823289295" class="phone"><i class="bx bx-phone"></i></a>
        <!-- <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a> -->
        <!-- <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a> -->
        <!-- <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a> -->
      </div>

    </div>
  </div>

  <div class="container footer-bottom clearfix">
    <div class="copyright">
      &copy; Copyright <strong><span>มังกรฟ้า ราชาโชค ล็อตเตอรี่ ออนไลน์</span></strong>. All Rights Reserved
    </div>
    <div class="credits">
      <!-- All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: https://bootstrapmade.com/license/ -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/vlava-free-bootstrap-one-page-template/ -->
      Designed by <a href="">Onesittichok</a>
    </div>
  </div>
</footer><!-- End Footer -->
