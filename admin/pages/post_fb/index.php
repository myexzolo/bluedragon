<!DOCTYPE html>
  <?php
  header("Content-type:text/html; charset=UTF-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);

  $pageName     = "โพสต์ Facebook";
  $pageCode     = "";
  ?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ระบบ Back Office - <?= $pageName ?></title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/post_fb.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php
          include("../../inc/sidebar.php");

          $sql   = "SELECT * FROM t_fb_post";

          $query      = DbQuery($sql,null);
          $row        = json_decode($query, true);
          $rows       = $row['data'];


          $code_embed = @$rows[0]['code_embed'];
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              <?= $pageName ?>
              <small><?=$pageCode ?></small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-home"></i> หน้าหลัก</a></li>
              <li class="active"><?= $pageName ?></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php"); ?>
            <!-- Main row -->
            <!-- <form novalidate action="ajax/AED.php" method="post"> -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title"></h3>
                <?php if($_SESSION['ROLE_USER']['is_insert'])
                {
                ?>
                  <a class="btn btn-social btn-success btnAdd pull-right" onclick="showForm('ADD')">
                    <i class="fa fa-plus"></i> โพสต์
                  </a>
                <?php
                }
                ?>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div id="showTable"></div>
              </div>
            </div>
            <!-- /.row -->
          </section>>


          <!-- /.content -->
        </div>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">โพสต์ Facebook</h4>
              </div>
              <form id="formAED" novalidate enctype="multipart/form-data">
              <!-- <form novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                <div id="show-form"></div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/post_fb.js"></script>
    </body>
  </html>
