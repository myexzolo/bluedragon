<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>Num</th>
      <th>Code</th>
      <th>Name</th>
      <th>รายละเอียด</th>
      <th>สถานะ</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
        <th style="width:70px;">Edit</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
        <th style="width:70px;">Del</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $str    = $_SESSION['member'][0]['user_id']==0?"":"AND role_id > 0";
      $sqls   = "SELECT * FROM t_role ORDER BY role_id DESC";

      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

        foreach ($rows as $key => $value) {
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td><?=$value['role_code'];?></td>
      <td><?=$value['role_name'];?></td>
      <td><?=$value['role_desc'];?></td>
      <td><?=$value['is_active']=='Y'?"ใช้งาน":"ไม่ใช้งาน";?></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['role_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="delModule('<?=$value['role_id']?>','<?=$value['role_name']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
