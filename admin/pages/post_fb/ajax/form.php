<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id = $_POST['id'];

if($action == 'EDIT'){
  $btn = 'Update changes';

  $sqls   = "SELECT * FROM t_fb_post where fb_post_id = '$id'";

  $query      = DbQuery($sqls,null);
  $row        = json_decode($query, true);
  $rows       = $row['data'];


  $fb_post_url = $rows[0]['fb_post_url'];
  $fb_post_seq = $rows[0]['fb_post_seq'];
  $is_active    = $rows[0]['is_active'];
}
if($action == 'ADD'){
 $btn = 'Save changes';
}
?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="fb_post_id" value="<?=@$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label>URL ของโพสต์</label>
        <input value="<?=@$fb_post_url?>" name="fb_post_url" type="text" class="form-control" placeholder="URL" required required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>ลำดับการแสดง</label>
        <input value="<?=@$fb_post_seq?>" OnKeyPress="return chkNumber(this)"  name="fb_post_seq" type="text" maxlength="3" class="form-control" placeholder="Sequence" required required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>สถานะ</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
