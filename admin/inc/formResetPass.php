<?php
include('function/connect.php');
include('function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$member = $_SESSION['member'];
$id = $member[0]['user_id'];




  $sql   = "SELECT * FROM t_user WHERE user_id = '$id'";
    //echo $sql;
  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  $user_login           = $row[0]['user_login'];
  $user_id              = $row[0]['user_id'];
  $user_name            = $row[0]['user_name'];
  if(isset($row[0]['user_last']) && $row[0]['user_last'] != ""){
    $user_name = $user_name." ".$row[0]['user_last'];
  }
?>
<input type="hidden" id="action" name="action" value="RESET">
<input type="hidden" name="user_id" value="<?=$user_id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label>ชื่อ</label>
        <input value="<?=@$user_name?>" type="text" class="form-control" placeholder="Name" required readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>User Login</label>
        <input value="<?=@$user_login?>" type="text" class="form-control" placeholder="User Login" required readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Reset Password</label>
        <input value="" name="user_password" id="pass1" type="password" required data-smk-msg="&nbsp;" autocomplete="new-password"  class="form-control" placeholder="Password" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Confirm Password</label>
        <input value="" name="cfm_user_password" id="pass2" type="password" required data-smk-msg="&nbsp;" autocomplete="new-password"  class="form-control" placeholder="Confirm Password" required>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
