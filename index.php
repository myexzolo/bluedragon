<!DOCTYPE html>
<html lang="en">
<?php
include('admin/inc/function/mainFunc.php');
include('admin/inc/function/connect.php');

$sqls   = "SELECT * FROM t_fb_pixel";

$query      = DbQuery($sqls,null);
$row        = json_decode($query, true);
$rows       = $row['data'];


$pixel_id = @$rows[0]['pixel_id'];

?>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>มังกรฟ้า ราชาโชค - Index</title>
  <meta content="มังกรฟ้า ราชาโชค ล็อตเตอรี่ ออนไลน์  rachachok" name="description">
  <meta content="มังกรฟ้า ราชาโชค ล็อตเตอรี่ ออนไลน์ rachachok" name="keywords">
  <meta data-n-head="ssr" data-hid="description" name="description" content="มังกรฟ้า ราชาโชค ล็อตเตอรี่ ออนไลน์ แหล่งรวมล็อตเตอรี่ที่ใหญ่ที่สุดในไทย - ขายล็อตเตอรี่ออนไลน์, ขายส่งล็อตเตอรี่, ตรวจล็อตเตอรี่, เลขเด็ด, rachachok">
  <meta data-n-head="ssr" data-hid="apple-mobile-web-app-title" name="apple-mobile-web-app-title" content="มังกรฟ้า ราชาโชค ล็อตเตอรี่ ออนไลน์ แหล่งรวมล็อตเตอรี่ที่ใหญ่ที่สุดในไทย - ขายล็อตเตอรี่ออนไลน์, ขายส่งล็อตเตอรี่, ตรวจล็อตเตอรี่, เลขเด็ด, rachachok">
  <meta name="google-site-verification" content="BYsJAWlE6UGGMEHhsUOBIN2cOQ4Jty9mGs-dvr1"/>
  <!-- Favicons -->
  <link href="assets/img/rachachok.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">


  <!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '<?=$pixel_id?>');
    fbq('track', 'PageView');
  </script>
  <noscript>
    <img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=<?=$pixel_id?>&ev=PageView&noscript=1"/>
  </noscript>
  <!-- End Facebook Pixel Code -->

    <style>
   .facebook
   {
     background: #4267B2 !important;
     font-size: 35px !important;
     width: 50px !important;
     height: 50px !important;
     color: #fff;

   }

   .facebook:hover
   {
     background: #3b5998 !important;
   }

   .line
   {
     padding-top: 11px !important;
     background: #00B900 !important;
     font-size: 35px !important;
     width: 50px !important;
     height: 50px !important;
     color: #fff;

   }

   .phone
   {
     background: #25D366 !important;
     font-size: 35px !important;
     width: 50px !important;
     height: 50px !important;
     color: #fff;
   }

   .linkBuy
   {
     color: #000000;
     text-decoration: underline;
   }

   .linkBuy:hover
   {
     color: #ff6f00;
     text-decoration: underline;
   }

   .salelotto
   {
     color: #ff6f00;
     font-size:17px
     ;margin-top: -5px;
   }

   @media (max-width: 768px) {
     .salelotto
     {
       font-size:16px
     }

     .facebook-video
     {
       height: 696px;
     }
   }

   @media (max-width: 1024px) {
     .salelotto
     {
       font-size:16px
     }

     .facebook-video
     {
       height: 696px;
     }
   }

   @media (max-width: 500px) {

     .facebook-video
     {
       height: 391px;
     }
   }
  </style>
  <!-- =======================================================
  * Template Name: Vlava - v4.3.0
  * Template URL: https://bootstrapmade.com/vlava-free-bootstrap-one-page-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
  <!-- ======= Header ======= -->
  <?php
  $url_dealers = "https://bluedragonlottery.com/dealers/%E0%B8%93%E0%B8%B1%E0%B8%8E%E0%B8%90%E0%B8%B2%E0%B8%A1%E0%B8%93%E0%B8%B5_%E0%B8%AD%E0%B8%B2%E0%B8%93%E0%B8%B2%E0%B8%88%E0%B8%B1%E0%B8%81%E0%B8%A3%E0%B8%A5%E0%B9%87%E0%B8%AD%E0%B8%95%E0%B9%80%E0%B8%95%E0%B8%AD%E0%B8%A3%E0%B8%B5%E0%B9%88__%E0%B8%A1%E0%B8%B1%E0%B8%87%E0%B8%81%E0%B8%A3%E0%B8%9F%E0%B9%89%E0%B8%B2-epjiu0b6t4";
  include('header.php');
  ?>
  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">
      <div class="text">
      <h1>สังคมใหม่ ให้ชีวิตไม่มีสิ้นหวัง มังกรฟ้า ราชาโชค</h1>
      <h2>ให้คุณเข้าถึงสลากกินแบ่งรัฐบาลได้ทุกที่ ทุกเวลา ให้เลือกอย่างจุใจ</h2>
      <a href="<?=$url_dealers ?>"  target="_blank" class="btn-get-started scrollto">
        ซื้อล็อตเตอรี่ออนไลน์
      </a>
      <a href="#footer" class="btn-get-started scrollto">
        ติดต่อเรา</a>
    </div>
      <video src="assets/video/space.mov" muted loop autoplay  playsinline><video>
      <div class="overlay"></div>

    </div>
  </section><!-- End Hero -->
  <main id="main">
    <!-- ======= About Section ======= -->
    <?php include('about.php'); ?>
    <?php include('blog.php'); ?>
    <!-- ======= Services Section ======= -->
    <?php include('services.php'); ?>
    <!-- ======= Featured Section ======= -->
    <?php //include('featured.php'); ?>
    <!-- ======= Testimonials Section ======= -->
    <?php //include('testimonials.php'); ?>
    <!-- ======= Portfolio Section ======= -->
    <?php //include('portfolio.php'); ?>
    <!-- ======= Team Section ======= -->
    <?php //include('team.php'); ?>
    <!-- ======= Frequently Asked Questions Section ======= -->
    <?php //include('faq.php'); ?>
    <!-- ======= Contact Section ======= -->
    <?php //include('contact.php'); ?>
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <?php include('footer.php'); ?>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>
