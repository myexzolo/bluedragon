
<!-- jQuery 3 -->
<script src="../../osc/js/jquery.min.js"></script>
<script src="../../osc/js/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../osc/js/bootstrap.min.js"></script>
<!-- dataTables -->
<script src="../../osc/js/jquery.dataTables.js"></script>
<script src="../../osc/js/dataTables.bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../../osc/js/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../osc/js/adminlte.min.js"></script>
<!-- Sparkline -->
<script src="../../osc/js/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="../../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="../../osc/js/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="../../osc/js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--script src="../../osc/js/pages/dashboard2.js"></script-->
<!-- AdminLTE for demo purposes -->
<script src="../../osc/js/demo.js"></script>
<!-- smoke -->
<script src="../../osc/js/smoke.js"></script>
<!-- owl -->
<script src="../../osc/js/owl.carousel.js"></script>
<script src="../../osc/js/lightbox.min.js"></script>
<!-- ckeditor -->

<!-- select2 -->
<script src="../../osc/js/select2.full.min.js"></script>

<!-- datepicker -->
<script src="../../osc/js/bootstrap-datepicker.js"></script>
<script src="../../osc/js/bootstrap-datepicker-thai.js"></script>
<script src="../../osc/js/locales/bootstrap-datepicker.th.js"></script>

<!-- datetimepicker -->
<script src="../../osc/js/moment.min.js"></script>
<script src="../../osc/js/daterangepicker.js"></script>
<script src="../../osc/js/nl.js"></script>
<script src='../../osc/js/bootstrap-datetimepicker.min.js'></script>

<!-- bootstrap-year-calendar -->
<script src='../../osc/js/bootstrap-year-calendar.min.js'></script>


<!-- dropzone -->
<script src="../../osc/js/dropzone.js"></script>
<script src='../../osc/js/bootstrap-select.js'></script>
<script src='../../osc/js/bootstrap-colorpicker.min.js'></script>

<script src='../../osc/js/Chart.min.js'></script>
<script src='../../osc/js/utils.js'></script>
<script src='../../osc/js/canvasjs.min.js'></script>
<script src='../../osc/js/jquery-jvectormap-2.0.5.min.js'></script>


<script src='../../osc/js/jquery-sortable-photos.js'></script>

<!-- mainFunc -->
<script src="../../osc/js/mainFunc.js"></script>


<script type="text/javascript">

function logout(){
  // alert(1);
  $.smkConfirm({
    text:'ยืนยันออกจากระบบ ?',
    accept:'ยืนยัน',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
     window.location='../../pages/login/';
    }
  });
}
function changeBranch(){
  var branchCode = $('#branchCodeSlidebar option:selected').val();
  $.post("../../inc/function/setbranch.php",{branchCode:branchCode})
    .done(function( data ) {
      location.reload();
  });
}

function resetPassword(){
  $.get("../../inc/formResetPass.php")
    .done(function( data ) {
      $('#myModalResetPassword').modal({backdrop:'static'});
      $('#form-rw').html(data);
  });
}

$('#formResetPwHeader').on('submit', function(event) {
  event.preventDefault();
  if ($('#formResetPwHeader').smkValidate()) {
    if( $.smkEqualPass('#pass1', '#pass2') ){
      $.ajax({
          url: '../../inc/resetPw.php',
          type: 'POST',
          data: new FormData( this ),
          processData: false,
          contentType: false,
          dataType: 'json'
      }).done(function(data) {
          $.smkAlert({text: data.message,type: data.status});
          $('#formResetPwHeader').smkClear();
          $('#myModalResetPassword').modal('toggle');
      }).fail(function (jqXHR, exception) {
          $('#formResetPwHeader').smkClear();
          $.smkAlert({text: "ไม่สามารถเปลี่ยนรหัสผ่านได้ !!",type: "danger"});
          $('#myModalResetPassword').modal('toggle');
      });
    }
  }
});


</script>
