<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
require_once '../../../PHPMailer/PHPMailerAutoload.php';

$action           = isset($_POST['action'])?$_POST['action']:"";
$installment_id   = isset($_POST['installment_id'])?$_POST['installment_id']:"";
$mem_email        = isset($_POST['mem_email'])?$_POST['mem_email']:"";
$transfer_price   = isset($_POST['transfer_price'])?number_format($_POST['transfer_price'],2):"0.00";

$transfer_img    = @$_FILES['transfer_img'];
$pathUpload       = "../../../../images/slip";

//print_r($_POST);
unset($_POST["action"]);
unset($_POST["mem_email"]);
if($action == "EDIT")
{
  $t_img = uploadfile($transfer_img,$pathUpload,"t_");
  $_POST['transfer_img'] = $t_img['image'];
  $_POST['transfer_status'] = "Y";
  $_POST['transfer_date'] = date("Y-m-d H:i:s");

  $pathTransfer =  $pathUpload."/".$t_img['image'];

  $sql = DBUpdatePOST($_POST,'order_installment','installment_id');
}
// echo $sql;
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($errorInfo[0]) == 0){

  if($mem_email != "")
  {
    $sql2 = "SELECT j.tj_name,o.*, m.mem_email
            FROM order_installment o, order_detail od , orders ods, t_job j,t_member m
            WHERE o.status_pay = 'Y'
            and o.od_id = od.od_id
            and od.o_id = ods.o_id
            AND od.mem_id = m.mem_id
            and ods.tj_id = j.tj_id
            and j.tj_status not in ('E')
            and o.installment_id = $installment_id
            order By o.date_update";

    $query  = DbQuery($sql2,null);
    $json   = json_decode($query, true)['data'][0];


    $message   = "<b>แจ้งยอดโอนเงินงวดที่ ".$json['installment_no']."<bb><br>";
    $message  .= "<b>Project : </b>".$json['tj_name']."<br>";
    $message  .= "<b>ยอดเงิน : </b>".$transfer_price."<br>";

    $arr = array();
    $arr['title']     = "ARCCO-TH แจ้งยอดโอนเงินงวดที่ ".$json['installment_no'];
    $arr['headerEmail']  = "ARCCO-TH";
    $arr['message']   = $message;
    $arr['email']     = $mem_email;
    $arr['attachment']['path'] = $pathTransfer;
    $arr['attachment']['newname'] = randomString(6,2);

      mailsendMail($arr);
  }
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>



?>
