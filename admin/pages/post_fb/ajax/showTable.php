<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px;">ลำดับ</th>
      <th>URL ของโพสต์</th>
      <th>โพสต์</th>
      <th style="width:120px;">ลำดับการแสดง</th>
      <th style="width:120px;">สถานะ</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
        <th style="width:70px;">Edit</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
        <th style="width:70px;">Del</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT * FROM t_fb_post where is_active <> 'D' ORDER BY fb_post_seq";
      $querys = DbQuery($sqls,null);
      $row    = json_decode($querys, true);
      $rows   = $row['data'];
      if($row['dataCount'] > 0){
        foreach ($rows as $key => $value) {
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td><?=$value['fb_post_url'];?></td>
      <td>
        <div class="fb-post"
            data-href="<?=$value['fb_post_url'];?>"
        </div>
      </td>
      <td><?=$value['fb_post_seq'];?></td>
      <td><?=$value['is_active']=='Y'?"ใช้งาน":"ไม่ใช้งาน";?></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['fb_post_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="delModule('<?=$value['fb_post_id']?>','')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php } }?>
  </tbody>
</table>
<script>
  $(function () {
    $(function () {
      $('#tableDisplay').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : false,
        'info'        : true,
        'autoWidth'   : false
      })
    })
  })
</script>
