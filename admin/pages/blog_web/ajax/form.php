<style>
.form-control.custom-file-input{
  padding: 1px;
}

input[type="file"] {
    display: block;
    text-align: right;
}

input[type=file]:focus, input[type=checkbox]:focus, input[type=radio]:focus {
    outline: 0px auto -webkit-focus-ring-color !important;
    /* outline-offset: -2px; */
}

.custom-file-input::-webkit-file-upload-button {
  visibility: hidden;
}
.custom-file-input::before {
  content: 'เลือกไฟล์';
  display: inline-block;
  background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
  border: 1px solid #999;
  border-radius: 0px;
  padding: 5px 15px;
  outline: none;
  white-space: nowrap;
  -webkit-user-select: none;
  cursor: pointer;
  text-shadow: 1px 1px #fff;
  font-weight: 600;
  font-size: 17px;
  position: absolute;
}
.custom-file-input:hover::before {
  border-color: black;
}
.custom-file-input:active::before {
  background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
}

.select2-hidden-accessible {
    border: 0 !important;
    clip: rect(0 0 0 0) !important;
    height: 3px !important;
    overflow: hidden !important;
    padding: 0 !important;
    position: relative !important;
    width: 1px !important;
}
</style>

<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id     = $_POST['id'];

if($action == 'EDIT'){
  $btn = 'Update changes';

  $sqls   = "SELECT * FROM t_post where post_id = '$id'";

  $query      = DbQuery($sqls,null);
  $row        = json_decode($query, true);
  $rows       = $row['data'];


  $post_url   = $rows[0]['post_url'];
  $post_title = $rows[0]['post_title'];
  $post_seq   = $rows[0]['post_seq'];
  $post_img   = "../../../images/articles/".$rows[0]['post_img'];
  $is_active  = $rows[0]['is_active'];
}
if($action == 'ADD'){
 $btn = 'Save changes';
}
?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="post_id" value="<?=@$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label>ข้อความ</label>
        <input value="<?=@$post_title?>" name="post_title" type="text" class="form-control" placeholder="" required required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>URL บทความ</label>
        <input value="<?=@$post_url?>" name="post_url" type="text" class="form-control" placeholder="URL" required required data-smk-msg="&nbsp;">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>รูปภาพ (ไฟล์ไม่เกิน 3 MB)</label>
        <input name="post_img" type="file" style="width:100%" class="form-control custom-file-input" onchange="readURL(this,'showImg');" accept="image/x-png,image/gif,image/jpeg">
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group" id="showImg" style="margin-bottom:0px;margin-top: 15px;">
          <img src="<?= @$post_img ?>"  onerror="this.onerror='';this.src='../../image/no-image.jpg'" style="height: 60px;">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>ลำดับการแสดง</label>
        <input value="<?=@$post_seq?>" OnKeyPress="return chkNumber(this)" name="post_seq" type="text" maxlength="3" class="form-control" placeholder="Sequence" required required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>สถานะ</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
