<style>
.iframe-container {
  overflow: hidden;
  /* 16:9 aspect ratio */
  padding-top: 56.25%;
  position: relative;
}

.iframe-container iframe {
   height: 100%;
   left: 0;
   position: absolute;
   top: 0;
}

.pricing .box h3 {
    font-size: 16px;
}
</style>

<?php

$sql         = "SELECT * FROM t_fb_post where is_active = 'Y' ORDER BY fb_post_seq";
$query       = DbQuery($sql,null);
$json        = json_decode($query, true);
$postFBData  = $json['data'];
$postFBCount = $json['dataCount'];


$sql        = "SELECT * FROM t_post where is_active = 'Y' ORDER BY post_seq";
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$postData   = $json['data'];
$postCount  = $json['dataCount'];


?>
<script src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>
<section id="blog" class="pricing">
  <div class="container">

    <div class="section-title">
      <h2>ข่าวหวยประจำวัน</h2>
      <!-- <p>Magnam dolores commodi suscipit eius consequatur ex aliquid fuga eum quidem</p> -->
    </div>
    <div class="row">
     <div class="col-md-12 col-lg-8">
       <div class="row">
         <?php
          for($x=0; $x < $postCount; $x++)
          {
            $featured = "";
            if($x%2 != 0)
            {
              $featured = "featured";
            }
         ?>
           <div class="col-lg-6 col-md-6">
             <div class="box <?=$featured ?>">
               <a data-v-73d2cb45 href="<?= $postData[$x]['post_url']?>" target="_blank">
                 <img src="images/articles/<?= $postData[$x]['post_img']?>"
                 data-src="images/articles/<?= $postData[$x]['post_img']?>"
                 alt="<?= $postData[$x]['post_title']?>" class="object-cover w-full" style="width: 100%;" lazy="loaded">
                 <h3><?= $postData[$x]['post_title']?></h3>
               </a>
             </div>
           </div>
         <?php
          }
         ?>
        </div>
      </div>
      <div class="col-md-12 col-lg-4">
        <div class="row">
          <?php
           for($x=0; $x < $postFBCount; $x++)
           {
          ?>
            <div class="col-md-12" align="center" style="margin-bottom:10px;min-height:200px;">

              <div class="fb-post"
                  data-href="<?= $postFBData[$x]['fb_post_url']?>"
                  data-width="auto" >
              </div>
            </div>
          <?php
           }
          ?>
          <div class="col-md-12 " align="center">
            <a target="_blank" href="https://www.facebook.com/mongkornfahrachachok/" style="color:#ff6f00;text-decoration:underline">อ่านเพิ่ม</a>
          </div>
      </div>
    </div>
  </div>
</section><!-- End Pricing Section -->
